These SVGs are free for your re-use, with two restrictions:

a) `baran.svg` is a *derived* work from Baran's RAND memorandum from 1964,
  "[On Distributed Communications](https://www.rand.org/pubs/research_memoranda/RM3420.html)".
  We cannot claim any rights on the original illustration; this derived
  SVG is released as [CC0 "No rights reserved"](https://creativecommons.org/public-domain/cc0/)
b) The other files starting with the `redistributed-` prefix are hereby
   released under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
   license.
