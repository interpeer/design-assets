# Design Assets

Companion to [design-brief](/interpeer/design-brief), assets for re-use in other projects.

Copyright (c) 2022 [Interpeer gUG](https://interpeer.io/).
[License terms](./LICENSE) apply for use. Different terms are usually fine,
just ask before use.
