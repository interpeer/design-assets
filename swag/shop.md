Texts for [the shop](https://www.freewear.org/InterpeerProject).

= Main =
URL: https://www.freewear.org/InterpeerProject

The Interpeer Project’s objective is to empower a human centric internet.
This currently takes the form of a technological initiative with the goal
of providing a secure and efficient
[information-centric networking](https://interpeer.io/knowledge-base/information-centric-networking/)
stack that protects human rights by design.

In the words of one Tor developer: "you want to make us obsolete!"

This breaks down into a growing number of component
[projects](https://interpeer.io/projects/), that you can help with – by
contributing [code](https://codeberg.org/interpeer/) or if you can prefer,
you can [donate](https://interpeer.io/donations). Every cent will go 100%
to our [mission](https://interpeer.io/about/mission-statement-of-the-the-interpeer-project/).

Of course, by far the most stylish way to donate is to grab some of the
swag right here in this shop!

Since 2023, there has been a close collaboration between the Interpeer Project and
FreeWear.org, that donate a portion of the sales of Interpeer materials in this
website. We also participate in Interpeer Project events.

More information: [https://interpeer.io/](https://interpeer.io/)

= T-Shirt: Privacy =
URL: https://www.freewear.org/FW0682--Interpeer-Project-Privacy-T-shirt

"Any sufficiently useful telemetry is indistinguishable from privacy invasion"

With this riff on [Clarke's third law](https://en.wikipedia.org/wiki/Clarke's_three_laws),
you can show off your knowledge of one of science fiction's greatest authors
*and* protest surveillance capitalism.

= T-Shirt: Ethical =
URL: https://www.freewear.org/FW0681--Interpeer-Project-Ethical-T-shirt

"There is no ethical telemetry under capitalism"

Are you also tired of people ethicswashing consumer tracking as telemetry?
So are we.

It's harder than it seems to design a telemetry system in a way that truly
keeps your users' privacy protected. And surveillance capitalism constantly
pushes for *just a little more* data to be tracked.

This shirt re-imagines the well-known slogan for the Internet age.

  = T-Shirt: Friends =
URL: https://www.freewear.org/FW0680--Interpeer-Project-Friends-T-shirt

"Friends don't let friends use blockchain"

This shirt premiered at [The EU Open Source Policy Summit](https://openforumeurope.org/event/the-eu-open-source-policy-summit-2023/)
when Interpeer's founder wore an early version -- much to the delight of many
of the guests.

If your friends don't keep you from doing something you'll regret, what kind
of friends are they anyway?

= Mug: Interpeer =
URL: https://www.freewear.org/FW0683--Interpeer-Project-Mug

This is not a mug, it's a drinking [vessel](https://interpeer.io/projects/vessel).

And just like it's namesake can hold all kinds of data, this container will
serve all your beverage holding needs. You can fill the fixed-sided chunk as
much or as little as you'd like, and the white surface allows you to sign it
to show it's yours (pen not included).

It's an ideal container for transporting by arbitrary means, and it's fully
multi-user capable. You can even put a bunch of them in a row.

The similarities to the software truly are uncanny. The only thing the cup
is missing is encryption.
